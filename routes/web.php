<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/' ,'HomeController@index');
Route::get('/getindo' ,'HomeController@dataIndonesia')->name('api.indonesia');
Route::get('/getprovinsi' ,'HomeController@chartLaravel')->name('api.provinsi');
Route::get('/getnews' ,'HomeController@neWs')->name('api.new');
