<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Charts\CovidChart;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Http;

class HomeController extends Controller
{
    public function index()
    {
        try{
        $client = new Client(['headers' => ['content-type' => 'application/json'],]);
        $request = $client->get('' . config('global.endPointApi') . 'indonesia/provinsi')->getBody()->getContents();
        $data=json_decode($request);
        return view ('index')->with('data', $data);
        }catch (RequestException $e) { } catch (\Exception $e) {
            return view ('index')->with('data', $data);
        }

    }

    public function dataIndonesia()
    {
        $client = new Client(['headers' => ['content-type' => 'application/json'],]);
        $request = $client->get('' . config('global.endPointApi') . 'indonesia')->getBody()->getContents();
        return response ()->json ( $request );

    }

    public function chartLaravel()
    {
        // $client = new Client(['headers' => ['content-type' => 'application/json'],]);
        // $request = $client->get('' . config('global.endPointApi') . 'indonesia/provinsi')->getBody()->getContents();
        // return response ()->json ( $request );
        $suspects = collect(Http::get('https://api.kawalcorona.com/indonesia/provinsi')->json());
        
        $labels = $suspects->flatten(1)->pluck('Provinsi');
        $data   = $suspects->flatten(1)->pluck('Kasus_Posi');
        $colors = $labels->map(function($item) {
            return $rand_color = '#' . substr(md5(mt_rand()), 0, 6);
        });

        $chart = new CovidChart;
        $chart->labels($labels);
        $chart->dataset('Kasus Positif', 'line', $data)->backgroundColor($colors);

        return view('corona', [
            'chart' => $chart,
        ]);
    }

    public function neWs()
    {
        $client = new Client(['headers' => ['content-type' => 'application/json'],]);
        $request = $client->get('http://newsapi.org/v2/top-headlines?country=id&category=science&apiKey=4c46873d724c4775830de8a61ee01311')->getBody()->getContents();
        return response ()->json ( $request );
    }
}
