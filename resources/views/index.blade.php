<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

<title>Covid-19 - HTML 5 Template Preview</title>

<!-- Fav Icon -->
<link rel="icon" href="{{ asset('assets/images/pavicon.png')}}" type="image/x-icon">

<!-- Google Fonts -->

<link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&amp;family=Rubik:ital,wght@0,300;0,400;0,500;0,700;0,900;1,300;1,400;1,500;1,700;1,900&amp;display=swap" rel="stylesheet">

<!-- Stylesheets -->
<link href="{{asset('assets/css/font-awesome-all.css')}}" rel="stylesheet">
<link href="{{asset('assets/css/owl.css')}}" rel="stylesheet">
<link href="{{asset('assets/css/bootstrap.css')}}" rel="stylesheet">
<link href="{{asset('assets/css/animate.css')}}" rel="stylesheet">
<link href="{{asset('assets/css/color.css')}}" rel="stylesheet">
<link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
<link href="{{asset('assets/css/responsive.css')}}" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet">

</head>


<!-- page wrapper -->
<body>

    <div class="boxed_wrapper">
        
        <!-- preloader -->
        <div class="preloader">
            <div id="handle-preloader" class="handle-preloader">
                <div class="animation-preloader">
                    <div class="spinner"></div>
                    <div class="txt-loading">
                        <span data-text-preloader="C" class="letters-loading">
                            C
                        </span>
                        <span data-text-preloader="O" class="letters-loading">
                            O
                        </span>
                        <span data-text-preloader="V" class="letters-loading">
                            V
                        </span>
                        <span data-text-preloader="I" class="letters-loading">
                            I
                        </span>
                        <span data-text-preloader="D" class="letters-loading">
                            D
                        </span>
                        <span data-text-preloader="-" class="letters-loading">
                            -
                        </span>
                        <span data-text-preloader="1" class="letters-loading">
                            1
                        </span>
                        <span data-text-preloader="9" class="letters-loading">
                            9
                        </span>
                    </div>
                </div>  
            </div>
        </div>
        <!-- preloader end -->


        <!-- main header -->
        <header class="main-header">
            <div class="outer-box clearfix">
                <div class="logo-box pull-left">
                    <figure class="logo"><a href="index.html"><img src="{{asset('assets/images/logo.png')}}" alt=""></a></figure>
                </div>
                <div class="menu-area pull-right">
                    <!--Mobile Navigation Toggler-->
                    <div class="mobile-nav-toggler">
                        <i class="icon-bar"></i>
                        <i class="icon-bar"></i>
                        <i class="icon-bar"></i>
                    </div>
                    <nav class="main-menu navbar-expand-md navbar-light">
                        <div class="collapse navbar-collapse show clearfix" id="navbarSupportedContent">
                            <ul class="navigation scroll-nav clearfix">
                                <li class="current"><a href="index.html">Home</a></li>
                                <li><a href="#Kasus">Kasus</a></li>
                                <li><a href="#Perlindungan">Perlindungan</a></li>
                                <li><a href="#kebiasaan">Kebiasaan</a></li>
                                <li><a href="#news">News</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>

        </header>
        <!-- main-header end -->

        <!-- Mobile Menu  -->
        <div class="mobile-menu">
            <div class="menu-backdrop"></div>
            <div class="close-btn"><i class="fas fa-times"></i></div>
            
            <nav class="menu-box">
                <div class="menu-outer"><!--Here Menu Will Come Automatically Via Javascript / Same Menu as in Header--></div>
                <div class="contact-info">
                    <h4>Contact Info</h4>
                    <ul>
                        <li>Jakarta barat</li>
                        <li><a href="tel:+8801682648101">082281803400</a></li>
                        <li><a href="mailto:info@example.com">oong.julian@gmail.com</a></li>
                    </ul>
                </div>
                <div class="social-links">
                    <ul class="clearfix">
                        <li><a href="index.html"><span class="fab fa-twitter"></span></a></li>
                        <li><a href="index.html"><span class="fab fa-facebook-square"></span></a></li>
                        <li><a href="index.html"><span class="fab fa-pinterest-p"></span></a></li>
                        <li><a href="index.html"><span class="fab fa-instagram"></span></a></li>
                        <li><a href="index.html"><span class="fab fa-youtube"></span></a></li>
                    </ul>
                </div>
            </nav>
        </div><!-- End Mobile Menu -->


        <!-- banner-section -->
        <section class="banner-section">
            <div class="anim-icon">
                <div class="icon icon-1 rotate-me" style="background-image: url(assets/images/icons/anim-icon-1.png);"></div>
                <div class="icon icon-2 rotate-me" style="background-image: url(assets/images/icons/anim-icon-2.png);"></div>
                <div class="icon icon-3 rotate-me" style="background-image: url(assets/images/icons/anim-icon-3.png);"></div>
            </div>
            <div class="shape-layer" style="background-image: url(assets/images/shape/shape-1.png);"></div>
            <div class="banner-carousel owl-theme owl-carousel owl-dots-none">
                <div class="slide-item">
                    <div class="auto-container">
                        <div class="row align-items-center clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12 content-column">
                                <div class="content-box">
                                    <h6>#DirumahAja</h6>
                                    <h1>Jaga diri anda dan keluarga anda.</h1>
                                    <p>Mari kita berdoa bersama agar covid-19 ini berakhir.</p>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 image-column">
                                <figure class="image-box"><img src="{{asset('assets/images/resource/banner-img-1.png')}}" alt=""></figure>
                            </div>
                        </div> 
                    </div>
                </div>
                <div class="slide-item">
                    <div class="auto-container">
                        <div class="row align-items-center clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12 content-column">
                                <div class="content-box">
                                    <h6>#DirumahAja</h6>
                                    <h1>Tetap bahagia walau dirumah aja.</h1>
                                    <p>Mari kita berdoa bersama agar covid-19 ini berakhir.</p>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 image-column">
                                <figure class="image-box"><img src="{{asset('assets/images/resource/banner-img-2.png')}}" alt=""></figure>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </section>
        <!-- banner-section end -->

        


        <!-- funfact-section -->
        <section class="funfact-section" id="Kasus">
            <div class="sec-title centred">
                <h2 style="padding-bottom: 150px;">Jumlah Kasus di Indonesia Saat Ini</h2>
            </div>
            <div class="auto-container">
                <div class="inner-container">
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-6 col-sm-12 counter-block">
                            <div class="counter-block-one">
                                <div class="inner-box">
                                    <figure class="icon-box"><img src="{{asset('assets/images/icons/icon-1.png')}}" alt=""></figure>
                                    <div class="count-outer count-box" id="positif">
                                        
                                    </div>
                                    <p>Total Positif</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12 counter-block">
                            <div class="counter-block-one">
                                <div class="inner-box">
                                    <figure class="icon-box"><img src="{{asset('assets/images/icons/icon-2.png')}}" alt=""></figure>
                                    <div class="count-outer count-box" id="sembuh">
                                    </div>
                                    <p>Total Sembuh</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12 counter-block">
                            <div class="counter-block-one">
                                <div class="inner-box">
                                    <figure class="icon-box"><img src="{{asset('assets/images/icons/icon-3.png')}}" alt=""></figure>
                                    <div class="count-outer count-box" id="meninggal">
                                    </div>
                                    <p>Total Meninggal</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- funfact-section end -->

        <section class="funfact-section" id="Kasus">
            <div class="sec-title centred">
                <h2 style="padding-bottom: 150px;padding-top: 150px;">Penyebaran Covid-19 di Provinsi</h2>
            </div>
            <div class="auto-container">
                <div class="inner-container">
                    <table id="example" class="row-border" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Provinsi</th>
                                <th>Kasus Positif</th>
                                <th>Kasus Sembuh</th>
                                <th>Kasus Meninggal</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 0;?>
                            @foreach ($data as $item)
                            <?php $no++ ;?>
                            <tr>
                                <td>{{ $no }}</td>
                                <td>{{ $item->attributes->Provinsi }}</td>
                                <td>{{ $item->attributes->Kasus_Posi }}</td>
                                <td>{{ $item->attributes->Kasus_Semb }}</td>
                                <td>{{ $item->attributes->Kasus_Meni }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Kode Provinsi</th>
                                <th>Kasus Positif</th>
                                <th>Kasus Sembuh</th>
                                <th>Kasus Meninggal</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </section>

        <!-- protect-section -->
        <section class="protect-section centred" id="Perlindungan">
            <div class="shape-layer" style="background-image: url(assets/images/shape/shape-2.png);"></div>
            <div class="anim-icon">
                <div class="icon icon-1 rotate-me" style="background-image: url(assets/images/icons/anim-icon-1.png);"></div>
                <div class="icon icon-2 rotate-me" style="background-image: url(assets/images/icons/anim-icon-3.png);"></div>
            </div>
            <div class="auto-container">
                <div class="sec-title">
                    <h6>Perlindungan</h6>
                    <h2>Ambil langkah dan lindungi dirimu</h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-4 col-md-6 col-sm-12 single-column">
                        <div class="single-item wow fadeInUp animated animated" data-wow-delay="00ms" data-wow-duration="1500ms">
                            <div class="inner-box">
                                <figure class="image-box"><img src="{{asset('assets/images/resource/protect-1.png')}}" alt=""></figure>
                                <h3>Gunakan Masker</h3>
                                <p>Menggunakan masker adalah salah satu cara untuk melindungi diri anda dari covid-19.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 single-column">
                        <div class="single-item wow fadeInUp animated animated" data-wow-delay="300ms" data-wow-duration="1500ms">
                            <div class="inner-box">
                                <figure class="image-box"><img src="{{asset('assets/images/resource/protect-2.png')}}" alt=""></figure>
                                <h3>Cuci Tangan</h3>
                                <p>Cuci tangan setelah anda berpergian dari luar, ini salah satu cara melindungi keluarga anda.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 single-column">
                        <div class="single-item wow fadeInUp animated animated" data-wow-delay="600ms" data-wow-duration="1500ms">
                            <div class="inner-box">
                                <figure class="image-box"><img src="{{asset('assets/images/resource/protect-3.png')}}" alt=""></figure>
                                <h3>Physical Distancing</h3>
                                <p>Pembatasan sosial atau menjaga jarak adalah serangkaian tindakan menghentikan atau memperlambat penyebaran penyakit menular.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="more-btn"><a href="index.html">How to Clean Hands?</a></div>
            </div>
        </section>
        <!-- protect-section end -->

        <!-- news-section -->
        <section class="news-section" id="news">
            <div class="anim-icon">
                <div class="icon icon-1 rotate-me" style="background-image: url(assets/images/icons/anim-icon-1.png);"></div>
                <div class="icon icon-2 rotate-me" style="background-image: url(assets/images/icons/anim-icon-3.png);"></div>
                <div class="icon icon-3 rotate-me" style="background-image: url(assets/images/icons/anim-icon-5.png);"></div>
            </div>
            <div class="auto-container">
                <div class="sec-title centred">
                    <h6>Article</h6>
                    <h2>Stay Updated with Our <br />News Feed.</h2>
                </div>
                <div class="row clearfix" id="paper">
                    
                </div>
            </div>
        </section>
        <!-- news-section end -->
<!-- handwash-section --> 
<section class="handwash-section centred" id="kebiasaan">
    <div class="bubble-icon">
        <div class="icon icon-1" style="background-image: url(assets/images/icons/bubble-icon-1.png);"></div>
        <div class="icon icon-2" style="background-image: url(assets/images/icons/bubble-icon-2.png);"></div>
        <div class="icon icon-3" style="background-image: url(assets/images/icons/bubble-icon-1.png);"></div>
        <div class="icon icon-4" style="background-image: url(assets/images/icons/bubble-icon-1.png);"></div>
        <div class="icon icon-5" style="background-image: url(assets/images/icons/bubble-icon-2.png);"></div>
    </div>
    <div class="auto-container">
        <div class="sec-title">
            <h6>Cuci Tangan Anda</h6>
            <h2>Cara Mencuci Tangan</h2>
        </div>
        <div class="inner-content clearfix">
            <div class="single-item">
                <div class="inner-box">
                    <figure class="image-box">
                        <img src="{{asset('assets/images/resource/hadnwash-1.png')}}" alt="">
                        <span class="count">1</span>
                    </figure>
                    <h6>Oleskan Sabun</h6>
                </div>
            </div>
            <div class="single-item">
                <div class="inner-box">
                    <figure class="image-box">
                        <img src="{{asset('assets/images/resource/hadnwash-2.png')}}" alt="">
                        <span class="count">2</span>
                    </figure>
                    <h6>Telapak dibasuh</h6>
                </div>
            </div>
            <div class="single-item">
                <div class="inner-box">
                    <figure class="image-box">
                        <img src="{{asset('assets/images/resource/hadnwash-3.png')}}" alt="">
                        <span class="count">3</span>
                    </figure>
                    <h6>Antara Jari</h6>
                </div>
            </div>
            <div class="single-item">
                <div class="inner-box">
                    <figure class="image-box">
                        <img src="{{asset('assets/images/resource/hadnwash-4.png')}}" alt="">
                        <span class="count">4</span>
                    </figure>
                    <h6>Bagian Belakang</h6>
                </div>
            </div>
            <div class="single-item">
                <div class="inner-box">
                    <figure class="image-box">
                        <img src="{{asset('assets/images/resource/hadnwash-5.png')}}" alt="">
                        <span class="count">5</span>
                    </figure>
                    <h6>Bersihkan dengan Air</h6>
                </div>
            </div>
            <div class="single-item">
                <div class="inner-box">
                    <figure class="image-box">
                        <img src="{{asset('assets/images/resource/hadnwash-6.png')}}" alt="">
                        <span class="count">6</span>
                    </figure>
                    <h6>Kengeringkan</h6>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- handwash-section end -->


        <!-- main-footer -->
        <footer class="main-footer">
            <div class="shape-layer" style="background-image: url(assets/images/shape/shape-3.png);"></div>
            <div class="anim-icon">
                <div class="icon icon-1 rotate-me" style="background-image: url(assets/images/icons/anim-icon-4.png);"></div>
                <div class="icon icon-2 rotate-me" style="background-image: url(assets/images/icons/anim-icon-3.png);"></div>
                <div class="icon icon-3 rotate-me" style="background-image: url(assets/images/icons/anim-icon-6.png);"></div>
            </div>
            <div class="footer-top">
                <div class="auto-container">
                    <div class="widget-section">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12 footer-column">
                                <div class="logo-widget footer-widget">
                                    <figure class="footer-logo"><a href="index.html"><img src="{{asset('assets/images/footer-logo.png')}}" alt=""></a></figure>
                                    <div class="text">
                                        <p>Covid-19 atau COVID-19 merupakan akronim dari coronavirus disease 2019. Coronavirus adalah keluarga virus yang beberapa di antaranya menyebabkan penyakit pada manusia, ada pula yang tidak. Virus korona tipe baru yang tengah menjadi pandemi ini bernama SARS-CoV-2 (severe acute respiratory syndrome-coronavirus-2). Virus inilah yang menyebabkan Covid-19.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 footer-column">
                                <div class="contact-widget footer-widget">
                                    <div class="widget-title">
                                        <h3>Contacts</h3>
                                    </div>
                                    <ul class="info-list clearfix">
                                        <li><i class="fas fa-map-marker-alt"></i>Semanan Kalideres kab.jakarta-barat -Jakarta</li>
                                        <li><i class="fas fa-microphone"></i><a href="tel:23055873407">082281803400</a></li>
                                        <li><i class="fas fa-envelope"></i><a href="mailto:info@example.com">oong.julian@gmail.com</a></li>
                                    </ul>
                                    <ul class="social-links clearfix">
                                        <li><a href="index.html"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a href="index.html"><i class="fab fa-twitter"></i></a></li>
                                        <li><a href="index.html"><i class="fab fa-vimeo-v"></i></a></li>
                                        <li><a href="index.html"><i class="fab fa-linkedin-in"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bottom">
                <div class="auto-container">
                    <div class="bottom-inner clearfix">
                        <div class="copyright pull-left">
                            <p><a href="index.html">Covid-19</a> &copy; 2020 All Right Reserved</p>
                        </div>
                        <ul class="footer-nav pull-right">
                            <li><a href="index.html">by</a></li>
                            <li><a href="index.html">oong julian</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
        <!-- main-footer end -->


        <!--Scroll to top-->
        <button class="scroll-top scroll-to-target" data-target="html">
            <span class="fas fa-arrow-up"></span>
        </button>
    </div>

<!-- jequery plugins -->
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="{{asset('assets/js/jquery.js')}}"></script>
<script src="{{asset('assets/js/popper.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/owl.js')}}"></script>
<script src="{{asset('assets/js/wow.js')}}"></script>
<script src="{{asset('assets/js/appear.js')}}"></script>
<script src="{{asset('assets/js/scrollbar.js')}}"></script>
<script src="{{asset('assets/js/pagenav.js')}}"></script>

<!-- main-js -->
<script src="{{asset('assets/js/script.js')}}"></script>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script>
    $.ajax({
    type: 'GET',
    url: "{{ route('api.indonesia')}}",
    dataType: 'json',
    encode  : true,
    success: function (data) {
        var obj = JSON.parse(data);
        var positif = '<span class="count-text" data-speed="1500">'+obj[0].positif+'</span>';
            $("#positif").append(positif);
        var sembuh = '<span class="count-text" data-speed="1500">'+obj[0].sembuh+'</span>';
            $("#sembuh").append(sembuh);
        var meninggal = '<span class="count-text" data-speed="1500">'+obj[0].meninggal+'</span>';
            $("#meninggal").append(meninggal);
    },error:function(data){ 
        console.log(data)
    }
});

$.ajax({
    type: 'GET',
    url: "{{ route('api.new')}}",
    dataType: 'json',
    encode  : true,
    success: function (data) {
        var obj = JSON.parse(data);
        var datas=obj.articles;
        var i;
            for(i=0; i<datas.length; i++){
            var news='<div class="col-lg-4 col-md-6 col-sm-12 news-block">'+
                        '<div class="news-block-one wow fadeInUp animated animated" data-wow-delay="00ms" data-wow-duration="1500ms">'+
                            '<div class="inner-box">'+
                                '<figure class="image-box"><a href="blog-details.html"><img src="'+datas[i].urlToImage+'" alt=""></a></figure>'+
                                '<div class="lower-content">'+
                                    '<span class="feature">Featured</span>'+
                                    '<h3><a href="blog-details.html">'+datas[i].title+''+
                                    '<ul class="post-info clearfix">'+
                                        '<li>'+datas[i].author+'</li>'+
                                        '<li>'+datas[i].publishedAt+'</li>'+
                                    '</ul>'+
                                    '<p>'+datas[i].content+'</p>'+
                                    '<div><a target="_blank" href="'+datas[i].url+'">See Details</a></div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>';
                    $("#paper").append(news);
        }
    },error:function(data){ 
        console.log(data)
    }
});
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
</body><!-- End of .page_wrapper -->

</html>
